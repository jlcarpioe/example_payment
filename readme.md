# Example Payment
Proyecto de ejemplo para pagos de usuarios.

## Instalación

Dentro de la carpeta del proyecto realizar los siguientes pasos:

* Ejecutar el comando: `composer install` para realizar la instalación de los paquetes.
* Copiar el archivo `.env.example` y pegarlo con el nombre `.env` en caso de que sea la primera instalación. Se debe tener activo la opción de mostrar archivos ocultos.
* Dar permisos de lectura y escritura a las carpetas `storage` y `bootstrap/cache`. Se puede realizar con el comando:  `sudo chmod 777 -R storage bootstrap/cache`
* Ejecutar el comando `php artisan key:generate`
* Modificar los datos en el archivo `.env` para la conexión a la Base de Datos.
