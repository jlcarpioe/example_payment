<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// user operations
Route::get('/user', 'UserController@index');

Route::post('/user', 'UserController@store');

Route::delete('/user/{id}', 'UserController@destroy');

Route::put('/user/{id}', 'UserController@update');


// favorites operations
Route::post('/favorite', 'FavoriteController@store');

Route::delete('/favorite', 'FavoriteController@destroy');


// payments operations
Route::post('/payment', 'PaymentController@store');

Route::delete('/payment/{id}', 'PaymentController@destroy');