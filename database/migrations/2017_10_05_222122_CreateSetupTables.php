<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetupTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 * 
	 */
	public function up() {

		// Create table to store users
		Schema::create('usuarios', function (Blueprint $table) {
			$table->increments('codigousuario');
			$table->string('usuario', 64);
			$table->string('clave', 255);
			$table->integer('edad');
			$table->timestamps();
			$table->softDeletes();

			$table->index(['usuario', 'clave']);
		});

		// Create table to store favorites
		Schema::create('favoritos', function (Blueprint $table) {
			$table->integer('codigousuario')->unsigned();
			$table->integer('codigousuariofavorito')->unsigned();

			$table->foreign('codigousuario')->references('codigousuario')
				->on('usuarios')->onDelete('cascade');
			$table->foreign('codigousuariofavorito')->references('codigousuario')
				->on('usuarios')->onDelete('cascade');

			$table->primary(['codigousuario', 'codigousuariofavorito']);
		});

		// Create table to store payments
		Schema::create('pagos', function (Blueprint $table) {
			$table->increments('codigopago');
			$table->decimal('importe', 18, 2);
			$table->dateTime('fecha');
			$table->timestamps();
			$table->softDeletes();
		});

		// Create table to store relation of users and payments
		Schema::create('usuariospagos', function (Blueprint $table) {
			$table->integer('codigopago')->unsigned();
			$table->integer('codigousuario')->unsigned();

			$table->foreign('codigopago')->references('codigopago')->on('pagos')->onDelete('cascade');
			$table->foreign('codigousuario')->references('codigousuario')->on('usuarios')->onDelete('cascade');

			$table->primary(['codigopago', 'codigousuario']);

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('usuariospagos');
		Schema::dropIfExists('favoritos');
		Schema::dropIfExists('pagos');
		Schema::dropIfExists('usuarios');
	}

}
