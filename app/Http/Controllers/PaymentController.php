<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

use App\Models\Payment;
use App\Models\User;


class PaymentController extends Controller {
	

	/**
	 * Store a new payment in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		
		// validations
		$validator = Validator::make($request->all(), [
			'user_id' => 'required|exists:usuarios,codigousuario',
			'amount' => 'required|numeric|importe',
			'date'	=> 'required|date|after_or_equal:today',
		]);

		if ($validator->fails()) {
			return response()->json(['code'=>1, 'messages'=> $validator->errors()->all()], 400);
		}

		// save payment
		$payment = Payment::create([
			'importe' => $request->get('amount'),
			'fecha' => $request->get('date'),
		]);

		// assign payment to user
		$user = User::find($request->get('user_id'));
		$user->payments()->attach($payment->getKey());

		return response()->json($payment);
	}


	/**
	 * Remove the specified payment.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		
		try {
			Payment::destroy($id);
		} catch (\Exception $e) {
			return response()->json(['code'=>$e->getCode(), 'messages'=>$e->getMessage()], 500);	
		}

		return response()->json(['status'=>'ok']);
	}

}
