<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

use App\Models\User;


class UserController extends Controller {

	/**
	 * Display a listing of users.
	 *
	 * @return \Illuminate\Http\Response
	 * 
	 */
	public function index() {

		$user = User::with('favorites', 'payments')->get();

		return response()->json($user);
	}


	/**
	 * Store a new user in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		
		// validations
		$validator = Validator::make($request->all(), [
			'username' => 'required|unique:usuarios,usuario|max:64',
			'password'	=> 'required',
			'age'	=> 'required|integer|min:19',
		]);

		if ($validator->fails()) {
			return response()->json(['code'=>1, 'messages'=> $validator->errors()->all()], 400);
		}

		// save user info
		$user = User::create([
			'usuario' => $request->get('username'),
			'clave' => bcrypt($request->get('password')),
			'edad' => $request->get('age'),
		]);

		return response()->json($user);
	}


	/**
	 * Update the specified user in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		
		// validations
		$validator = Validator::make($request->all(), [
			'age'	=> 'integer|min:19',
		]);

		if ($validator->fails()) {
			return response()->json(['code'=>1, 'messages'=> $validator->errors()->all()], 400);
		}

		// find user
		$user = User::find($id);

		if (!$user) {
			return response()->json(['code'=>2, 'message'=> 'user_not_found'], 500);
		}

		$flag_change = FALSE;

		if ($request->filled('password')) {
			$user->clave = bcrypt($request->get('password'));
			$flag_change = TRUE;
		}

		if ($request->filled('age')) {
			$user->edad = $request->get('age');
			$flag_change = TRUE;
		}

		if ($flag_change) {
			$user->save();
		}

		return response()->json($user);
	}


	/**
	 * Remove the specified user from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		
		try {
			User::destroy($id);
		} catch (\Exception $e) {
			return response()->json(['code'=>$e->getCode(), 'messages'=>$e->getMessage()], 500);	
		}

		return response()->json(['status'=>'ok']);
	}

}
