<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

use App\Models\Favorite;


class FavoriteController extends Controller {

	/**
	 * Store a favorite user.
	 * 
	 * @param Request $request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {

		// validations
		$validator = Validator::make($request->all(), [
			'user_id' => 'required|integer|exists:usuarios,codigousuario',
			'favorite_id'	=> 'required|integer|different:user_id|exists:usuarios,codigousuario',
		]);

		if ($validator->fails()) {
			return response()->json(['code'=>1, 'messages'=> $validator->errors()->all()], 400);
		}

		try {
			// save info favorites
			Favorite::insert([
				'codigousuario' => $request->get('user_id'), 
				'codigousuariofavorito' => $request->get('favorite_id')
			]);
		} catch (\Exception $e) {
			return response()->json(['code'=>$e->getCode(), 'messages'=>$e->getMessage()], 500);
		}

		return response()->json(['status'=>'ok']);
	}


	/**
	 * Remove a relation of favorite.
	 *
	 * @param Request $request $request
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request) {
		
		try {

			Favorite::where([
				'codigousuario' => $request->get('user_id'), 
				'codigousuariofavorito' => $request->get('favorite_id')
			])
			->delete();

		} catch (\Exception $e) {
			return response()->json(['code'=>$e->getCode(), 'messages'=>$e->getMessage()], 500);	
		}

		return response()->json(['status'=>'ok']);
	}

}
