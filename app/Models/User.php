<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Model {

	use SoftDeletes;
	

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'usuarios';

	/**
	 * The primary key for the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'codigousuario';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['usuario', 'clave', 'edad'];

	/**
	 * The attributes that should be hidden for serialization.
	 *
	 * @var array
	 */
	protected $hidden = ['clave', 'deleted_at'];
	

	/**
	 * Get the favorite users for the user.
	 * 
	 */
	public function favorites() {
		return $this->hasMany('App\Models\Favorite', 'codigousuario');
	}

	/**
	 * The payments that belong to the user.
	 * 
	 */
	public function payments() {
		return $this->belongsToMany('App\Models\Payment', 'usuariospagos', 'codigousuario', 'codigopago');
	}

}
